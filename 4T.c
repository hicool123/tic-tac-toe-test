#include <gtk/gtk.h>

guint clicked_time = 0;
guint arry_win[]={0b111000000, 0b000111000, 0b0000000111, 0b100100100, 0b010010010, 0b001001001, 0b100010001, 0b001010100};
guint w_step, y_step = 0b000000000;
static void
callback_click_func (GtkWidget *widget,
             gpointer   data)
{

    const gchar *label_step = gtk_button_get_label((GtkButton*)widget);
    if( g_strcmp0(label_step, "💿️") != 0 && g_strcmp0(label_step, "📀️") != 0 )
    {
        if(clicked_time%2 == 0)
        {
            w_step |= (1 << (*label_step - '1'));
            g_print("label_step: %s; w_step:%x\n", label_step, w_step);
            gtk_button_set_label((GtkButton*)widget, "💿️");
            
            
        }
            
        else
        {
            y_step |= (1 << (*label_step - '1'));
            g_print("label_step: %s; y_step:%x\n", label_step, y_step);
            gtk_button_set_label((GtkButton*)widget, "📀️");
            
            
        }
        if(clicked_time >= 4)
        {
            for(int i = 0; i < 8; i++)
            {
                if((w_step & arry_win[i]) == arry_win[i] )
                {
                    g_print("w_step:%x,arry_win:%x;white win\n", w_step, arry_win[i]);
                    break;
                }
                else if((y_step & arry_win[i]) == arry_win[i] )
                {
                    g_print("y_step:%x,arry_win:%x;yellow win\n", y_step, arry_win[i]);
                    break;
                }
            }
        }
            clicked_time++;
    }
  
}

void callback_restart_func(void)
{

}

int
test (int   argc,
      char *argv[])
{
  GtkBuilder *builder;
  GObject *window;
  GObject *button;
  GError *error = NULL;

  gtk_init (&argc, &argv);
  gchar str_button[8] = {0};   
  /* Construct a GtkBuilder instance and load our UI description */
  builder = gtk_builder_new ();
  if (gtk_builder_add_from_file (builder, "4T.ui", &error) == 0)
    {
      g_printerr ("Error loading file: %s\n", error->message);
      g_clear_error (&error);
      return 1;
    }

  /* Connect signal handlers to the constructed widgets. */
  window = gtk_builder_get_object (builder, "window");
  g_signal_connect (window, "destroy", G_CALLBACK (gtk_main_quit), NULL);
    for(guint num_button = 1; num_button <= 9; num_button++)
    {
        g_snprintf(str_button, 8, "button%d", num_button);
        g_print("button:%s\n", str_button);
        button = gtk_builder_get_object (builder, str_button);
        g_signal_connect (button, "clicked", G_CALLBACK (callback_click_func), NULL);
    }
    button = gtk_builder_get_object (builder, "restart");
    g_signal_connect (button, "clicked", G_CALLBACK (callback_restart_func), NULL);
    gtk_main ();

  return 0;
}

int 
main(int   argc,
      char *argv[])
{
    return test(argc, argv);
}
